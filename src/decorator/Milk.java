package decorator;



class Milk extends CondimentDecorator {
    public Milk(Beverage beverage) {
        super(beverage);
    }

    public String getDescription() {
        return "Milk";
    }

    public Double cost() {
        return 0.50;
    }

    @Override
    public String getAdditionalInfo() {
        return null;
    }
}