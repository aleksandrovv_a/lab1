package without_pattern;

public class Ingredients {
    private String drinkName;
    private Double cost;
    private String additionalInfo;
    public Ingredients createMilk(String drinkName, Double cost){
        this.drinkName = drinkName;
        this.cost = cost;
        return this;
    }

    public Ingredients createChocolate(String drinkName, Double cost, String additionalInfo){
        this.drinkName = drinkName;
        this.cost = cost;
        this.additionalInfo = additionalInfo;
        return this;
    }

    public Ingredients createSugar(String drinkName, Double cost){
        this.drinkName = drinkName;
        this.cost = cost;
        return this;
    }

    public String getDrinkName() {
        return drinkName;
    }

    public Double getCost() {
        return cost;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }
}
