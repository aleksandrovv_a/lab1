package decorator;

class Espresso implements Beverage {
    public String getDescription() {
        return "Espresso";
    }
    public Double cost() {
        return 1.99;
    }

    public String getAdditionalInfo() {
        return null;
    }
}
