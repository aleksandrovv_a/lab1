package decorator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class DrinksDialog extends JDialog {
    private boolean addPressed = false;
    private boolean cancelPressed = false;
    private JCheckBox milkCheckbox;
    private JCheckBox chocolateCheckbox;
    private JCheckBox sugarCheckBox;

    private String[] options;

    public DrinksDialog(Frame parent, String beverage, String[] options) {
        super(parent, beverage + " Options", true);
        this.options = options;

        JPanel panel = new JPanel(new GridLayout(0, 1));

        milkCheckbox = new JCheckBox(options[0]);
        panel.add(milkCheckbox);

        chocolateCheckbox = new JCheckBox(options[1]);
        panel.add(chocolateCheckbox);

        sugarCheckBox = new JCheckBox(options[2]);
        panel.add(sugarCheckBox);

        JButton addButton = new JButton("Add");
        addButton.addActionListener(new AddButtonListener(this));
        panel.add(addButton);


        getContentPane().add(panel);
        setSize(new Dimension(200, 200));

        setLocationRelativeTo(parent);
    }

    public boolean isAddPressed() {
        return addPressed;
    }

    public boolean isMilkSelected() {
        return milkCheckbox.isSelected();
    }

    public boolean isChocolateSelected() {
        return chocolateCheckbox.isSelected();
    }

    public boolean isSugarSelected(){
        return sugarCheckBox.isSelected();
    }


    public void updateIngredients(List<String> newOptions) {
        this.options = newOptions.toArray(new String[0]);
        milkCheckbox.setText(newOptions.get(0));
        chocolateCheckbox.setText(newOptions.get(1));
        revalidate();
        repaint();
        pack();
    }

    private static class AddButtonListener implements ActionListener {
        private final DrinksDialog dialog;
        public AddButtonListener(DrinksDialog dialog) {
            this.dialog = dialog;

        }

        @Override
        public void actionPerformed(ActionEvent e) {
            dialog.addPressed = true;
            dialog.dispose();
        }
    }

}
