package decorator;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Coffee Order");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 600);

        JPanel mainPanel = new JPanel(new BorderLayout());
        frame.getContentPane().add(mainPanel);

        JPanel panel = new JPanel(new GridBagLayout());
        mainPanel.add(panel, BorderLayout.CENTER);

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.insets = new Insets(5, 5, 5, 5);

        JLabel label = new JLabel("Make an order");
        label.setFont(new Font("Arial", Font.BOLD, 12));
        constraints.gridx = 0;
        constraints.gridy = 0;
        panel.add(label, constraints);

        JButton buttonEspresso = new JButton("Espresso");
        buttonEspresso.setPreferredSize(new Dimension(120, 50));
        constraints.gridx = 1;
        constraints.gridy = 0;
        constraints.gridwidth = 1;
        panel.add(buttonEspresso, constraints);

        JButton buttonCappuccino = new JButton("Cappuccino");
        buttonCappuccino.setPreferredSize(new Dimension(120, 50));
        constraints.gridx = 2;
        constraints.gridy = 0;
        constraints.gridwidth = 1;
        panel.add(buttonCappuccino, constraints);


        JButton buttonLatte = new JButton("Latte");
        buttonLatte.setPreferredSize(new Dimension(120, 50));
        constraints.gridx = 3;
        constraints.gridy = 0;
        constraints.gridwidth = 1;
        panel.add(buttonLatte, constraints);

        String[] options = {"Add Milk", "Add Chocolate", "Add Sugar"};

        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Item");
        model.addColumn("Additional info");
        model.addColumn("Price");

        JTable table = new JTable(model);
        JScrollPane scrollPane = new JScrollPane(table);
        mainPanel.add(scrollPane, BorderLayout.SOUTH);


        //buton toOrder
        JButton buttonOrder = new JButton("to order");
        buttonOrder.setPreferredSize(new Dimension(80, 40));
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 1;
        panel.add(buttonOrder, constraints);

        buttonOrder.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double totalOrderPrice = 0.0;
                for (int i = 0; i < model.getRowCount(); i++) {
                    String itemName = (String) model.getValueAt(i, 0);
                    Object priceObj = model.getValueAt(i, 2);
                    // check, if there are name and price
                    if (itemName != null && !itemName.isEmpty() && priceObj instanceof Double) {
                        totalOrderPrice += (Double) priceObj;
                    }
                }
                JOptionPane.showMessageDialog(frame, "Total Order Price: $" + totalOrderPrice, "Order Summary", JOptionPane.INFORMATION_MESSAGE);
                model.setRowCount(0);
            }
        });

        buttonEspresso.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String drinkName = "Espresso";
                DrinksDialog dialog = new DrinksDialog(frame, drinkName, options);
                dialog.setVisible(true);

                if (dialog.isAddPressed()) {
                    List<String> selectedIngredients = new ArrayList<>();
                    if (dialog.isMilkSelected())
                        selectedIngredients.add("Milk");
                    if (dialog.isChocolateSelected())
                        selectedIngredients.add("Chocolate");
                    if (dialog.isSugarSelected())
                        selectedIngredients.add("Sugar");

                    Beverage beverage = new Espresso();
                    model.addRow(new Object[]{beverage.getDescription(), "", beverage.cost()});
                    double totalPrice = beverage.cost();

                    for (String ingredient : selectedIngredients) {
                        switch (ingredient) {
                            case "Milk":
                                beverage = new Milk(beverage);
                                model.addRow(new Object[]{" - Milk", "",beverage.cost()});
                                totalPrice += beverage.cost();
                                break;
                            case "Chocolate":
                                beverage = new Chocolate(beverage);
                                model.addRow(new Object[]{" - Chocolate", beverage.getAdditionalInfo(), beverage.cost()});
                                totalPrice += beverage.cost();
                                break;
                            case "Sugar":
                                beverage = new Sugar(beverage);
                                model.addRow(new Object[]{" - Sugar", "", beverage.cost()});
                                totalPrice += beverage.cost();
                                break;
                        }
                    }
                    model.addRow(new Object[]{"", "", totalPrice});
                    model.addRow(new Object[]{});

                    table.getColumnModel().getColumn(0).setCellRenderer(new CustomRenderer(table.getRowCount() - 3));
                    table.getColumnModel().getColumn(1).setCellRenderer(new CustomRenderer(table.getRowCount() - 3));
                    table.getColumnModel().getColumn(2).setCellRenderer(new CustomRenderer(table.getRowCount() - 3));

                }
            }
        });

        buttonLatte.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String drinkName = "Latte";
                DrinksDialog dialog = new DrinksDialog(frame, drinkName, options);
                dialog.setVisible(true);

                if (dialog.isAddPressed()) {
                    List<String> selectedIngredients = new ArrayList<>();
                    if (dialog.isMilkSelected())
                        selectedIngredients.add("Milk");
                    if (dialog.isChocolateSelected())
                        selectedIngredients.add("Chocolate");
                    if (dialog.isSugarSelected())
                        selectedIngredients.add("Sugar");

                    Beverage beverage = new Latte();
                    model.addRow(new Object[]{drinkName, beverage.getAdditionalInfo(), beverage.cost()});
                    double totalPrice = beverage.cost();

                    for (String ingredient : selectedIngredients) {
                        switch (ingredient) {
                            case "Milk":
                                beverage = new Milk(beverage);
                                model.addRow(new Object[]{" - Milk", "", beverage.cost()});
                                totalPrice += beverage.cost();
                                break;
                            case "Chocolate":
                                beverage = new Chocolate(beverage);
                                model.addRow(new Object[]{" - Chocolate", beverage.getAdditionalInfo(), beverage.cost()});
                                totalPrice += beverage.cost();
                                break;
                            case "Sugar":
                                beverage = new Sugar(beverage);
                                model.addRow(new Object[]{" - Sugar", "", beverage.cost()});
                                totalPrice += beverage.cost();
                                break;
                        }
                    }
                    model.addRow(new Object[]{"", "", totalPrice});
                    model.addRow(new Object[]{});

                    table.getColumnModel().getColumn(0).setCellRenderer(new CustomRenderer(table.getRowCount() - 3));
                    table.getColumnModel().getColumn(1).setCellRenderer(new CustomRenderer(table.getRowCount() - 3));
                    table.getColumnModel().getColumn(2).setCellRenderer(new CustomRenderer(table.getRowCount() - 3));

                }
            }
        });

        buttonCappuccino.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String drinkName = "Cappuccino";
                DrinksDialog dialog = new DrinksDialog(frame, drinkName, options);
                dialog.setVisible(true);

                if (dialog.isAddPressed()) {
                    List<String> selectedIngredients = new ArrayList<>();
                    if (dialog.isMilkSelected())
                        selectedIngredients.add("Milk");
                    if (dialog.isChocolateSelected())
                        selectedIngredients.add("Chocolate");
                    if (dialog.isSugarSelected())
                        selectedIngredients.add("Sugar");

                    Beverage beverage = new Cappuccino();
                    model.addRow(new Object[]{drinkName, "", beverage.cost()});
                    double totalPrice = beverage.cost();

                    for (String ingredient : selectedIngredients) {
                        switch (ingredient) {
                            case "Milk":
                                beverage = new Milk(beverage);
                                model.addRow(new Object[]{" - Milk", "", beverage.cost()});
                                totalPrice += beverage.cost();
                                break;
                            case "Chocolate":
                                beverage = new Chocolate(beverage);
                                model.addRow(new Object[]{" - Chocolate", beverage.getAdditionalInfo(), beverage.cost()});
                                totalPrice += beverage.cost();
                                break;
                            case "Sugar":
                                beverage = new Sugar(beverage);
                                model.addRow(new Object[]{" - Sugar", "", beverage.cost()});
                                totalPrice += beverage.cost();
                                break;
                        }
                    }
                    model.addRow(new Object[]{"", "", totalPrice});
                    model.addRow(new Object[]{});

                    table.getColumnModel().getColumn(0).setCellRenderer(new CustomRenderer(table.getRowCount() - 3));
                    table.getColumnModel().getColumn(1).setCellRenderer(new CustomRenderer(table.getRowCount() - 3));
                    table.getColumnModel().getColumn(2).setCellRenderer(new CustomRenderer(table.getRowCount() - 3));

                }
            }
        });


        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static class CustomRenderer extends DefaultTableCellRenderer {
        private final int boldRow;
        public CustomRenderer(int boldRow) {
            this.boldRow = boldRow;
        }
        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            // Проверяем, четная ли строка и находится ли она до boldRow
            if (row == boldRow) {
                label.setBackground(Color.LIGHT_GRAY);
            }
            if (row == table.getRowCount() - 1 && column == 1) {
                label.setFont(label.getFont().deriveFont(Font.BOLD));
            }
            return label;
        }
    }

}



