package without_pattern;

public class Drinks {
    private String drinkName;
    private Double cost;
    private String additionalInfo;

    public Drinks createEspresso(String drinkName, Double cost){
        this.drinkName = drinkName;
        this.cost = cost;
        return this;
    }

    public Drinks createCappucciono(String drinkName, Double cost){
        this.drinkName = drinkName;
        this.cost = cost;
        return this;
    }

    public Drinks createLatte(String drinkName, Double cost, String additionalInfo){
        this.drinkName = drinkName;
        this.cost = cost;
        this.additionalInfo = additionalInfo;
        return this;
    }

    public String getDrinkName() {
        return drinkName;
    }

    public Double getCost() {
        return cost;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }
}
