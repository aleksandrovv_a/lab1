package decorator;

class Chocolate extends CondimentDecorator {
    public Chocolate(Beverage beverage) {
        super(beverage);
    }
    public String getDescription() {
        return beverage.getDescription() + ", decorator.Chocolate";
    }
    public Double cost() {
        return 0.75;
    }

    public String getAdditionalInfo() {
        return "Белый с орехами";
    }
}