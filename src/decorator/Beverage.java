package decorator;

public interface Beverage {
     String getDescription();
     Double cost();
     String getAdditionalInfo();
}
