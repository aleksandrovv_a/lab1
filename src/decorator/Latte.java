package decorator;



public class Latte implements Beverage {
    @Override
    public String getDescription() {
        return "Latte";
    }

    @Override
    public Double cost() {
        return 3.42;
    }

    @Override
    public String getAdditionalInfo() {
        return "with almond milk";
    }
}
