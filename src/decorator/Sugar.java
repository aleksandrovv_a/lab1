package decorator;



class Sugar extends CondimentDecorator {
    public Sugar(Beverage beverage){
        super(beverage);
    }
    public String getDescription() {
        return beverage.getDescription() + ", decorator.Sugar";
    }

    public Double cost() {
        return 0.3;
    }

    @Override
    public String getAdditionalInfo() {
        return null;
    }
}
