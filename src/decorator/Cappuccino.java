package decorator;

class Cappuccino implements Beverage {
    public String getDescription() {
        return "Cappuccino";
    }

    public Double cost() {
        return 2.49;
    }

    @Override
    public String getAdditionalInfo() {
        return null;
    }
}
